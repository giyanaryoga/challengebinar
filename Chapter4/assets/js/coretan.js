/* DOM refresh */
const refresh = document.getElementById('restart')
  refresh.addEventListener('click', function (r) {
    location.reload()
})

/* DOM Hasil */
function resultDraw() {
  const draw = document.querySelector('#result')
  draw.innerHTML = 'Draw'
  draw.style.backgroundColor= '#035B0C'
  draw.style.textAlign = 'center'
  draw.style.color = '#fff'
  draw.style.fontWeight = '500'
  draw.style.fontSize = '36px'
  draw.style.fontFamily = 'arial'
  draw.style.paddingTop = '25px'
}

function resultCompWin() {
  const comWin = document.querySelector('#result')
  comWin.innerHTML = 'COM WIN'
  comWin.style.backgroundColor = '#4C9654'
  comWin.style.textAlign = 'center'
  comWin.style.color = '#fff'
  comWin.style.fontWeight = '500'
  comWin.style.fontSize = '36px'
  comWin.style.fontFamily = 'arial'
  comWin.style.paddingTop = '10px'
  comWin.style.paddingLeft = '20px'
  comWin.style.paddingRight = '20px'
  comWin.style.height = '130px'
}

function resultPlayWin() {
  const playWin = document.querySelector('#result')
  playWin.innerText = 'PLAYER WIN'
  playWin.style.backgroundColor = '#4C9654'
  playWin.style.textAlign = 'center'
  playWin.style.color = '#fff'
  playWin.style.fontWeight = '500'
  playWin.style.fontSize = '36px'
  playWin.style.fontFamily = 'arial'
  playWin.style.paddingTop = '10px'
  playWin.style.height = '130px'
}

/* Pilihan Computer */
function pilihanComp() {
  const pilihComp = document.querySelectorAll('.pilihanComp')
  const Batu = document.querySelector('.Batu')
  const Gunting = document.querySelector('.Gunting')
  const Kertas = document.querySelector('.Kertas')
  const random = Math.random()
  pilihComp.forEach(function (p) {
    p.className = 'pilihanComp'
  })
    if (random < 0.28){
      Batu.parentElement.classList.add('active')
      return Batu.className
    }
    else if (random >= 0.28 && random < 0.58){
      Gunting.parentElement.classList.add('active')
      return Gunting.className
    }
    else {
      Kertas.parentElement.classList.add('active')
      return Kertas.className
    }
}

function getHasil(player, comp) {
  //draw
  if (player === 'batu' && comp === 'Batu') { this.resultDraw() /*console.log('draw')*/}
  if (player === 'gunting' && comp === 'Gunting') {this.resultDraw()}
  if (player === 'kertas' && comp === 'Kertas') {this.resultDraw()}
  //lose
  if (player === 'gunting' && comp === 'Batu') {this.resultCompWin() /*console.log('lose')*/} 
  if (player === 'kertas' && comp === 'Gunting') {this.resultCompWin()}
  if (player === 'batu' && comp === 'Kertas') {this.resultCompWin()}
  //win
  if (player === 'kertas' && comp === 'Batu') {this.resultPlayWin() /*console.log('win')*/}
  if (player === 'batu' && comp === 'Gunting') {this.resultPlayWin()}
  if (player === 'gunting' && comp === 'Kertas') {this.resultPlayWin()}
}

//Pilih Player dan Objek Computer, Hasil
  const player = document.querySelector('#player')
  const pilih = document.querySelectorAll('.pilihPlayer')

  player.addEventListener('click', function (e) {
    const pilihPlayer = e.target.className
    const pilihComp = pilihanComp()
      pilih.forEach(function (p) {
        p.className = 'pilihPlayer'
      })
    e.target.parentElement.classList.add('active')
    if (e.target.parentElement.parentElement.classList.contains('active')){
      e.classList.remove('active')
    }
    console.log('Pilihan player : ', pilihPlayer)
    console.log('Pilihan computer : ', pilihComp)
    console.log('Hasil : ', getHasil(pilihPlayer, pilihComp))
  })


//pilihPlayer()
//pilihanComp()
//resultCompWin()
//resultDraw()
//resultPlayWin()
//pPlayer()
//putarComp()