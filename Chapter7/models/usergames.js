'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class UserGames extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGames.hasOne(models.UserBiodata, {
        foreignKey: 'user_id',
        as: 'userbiodata'
      })
      UserGames.hasOne(models.UserHistories, {
        foreignKey: 'user_id',
        as: 'userhistories'
      })
      UserGames.hasMany(models.Room, {
        foreignKey: 'player1',
        as: 'player1'
      })
      UserGames.hasMany(models.Room, {
        foreignKey: 'player2',
        as: 'player2'
      })
    }

    generateToken = () => {
      const playload = {
        id : this.id,
        username : this.username
      }

      const secret = "secret"
      const token = jwt.sign(playload, secret)

      return token
    }

    static #encrypt = (password) => {
      return bcrypt.hashSync(password, 10)
    }

    static register = (username, password) => {
      const encryptedPassword = this.#encrypt(password)
      console.log(encryptedPassword)
      this.create({username, password : encryptedPassword})
    }

    static authentication = async (username, password) => {
      let usergames = await this.findOne({where : {username} })
      if (usergames === undefined) return false

      if (!bcrypt.compareSync(password, usergames.password)){

        return Promise.reject("Username / password is incorrect")
      }
      return Promise.resolve(usergames)
    }
  };
  UserGames.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserGames',
  });
  return UserGames;
};