'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserHistories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      UserHistories.belongsTo(models.UserGames, {
        foreignKey: 'user_id',
        targetKey: 'id'
      })
    }
  };
  UserHistories.init({
    waktu: DataTypes.TIME,
    skor: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'UserHistories',
  });
  return UserHistories;
};