const router = require('express').Router()
const userBiodataController = require('../controllers/userBiodataController')
const restrict = require('../middleware/restrict');

router.get('/register', (req, res) => {
    res.render('register')
})

router.post('/register', authController.createUser)

router.get('/login', (req, res) => {
    res.render('login')
})

router.post('/login', authController.authUser)

router.get('/', (req, res) => {
    res.render('index')
})

router.get('/profile', restrict, (req, res) => {
    return res.json(req.user.username)
})

module.exports = router