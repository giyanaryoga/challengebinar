const router = require('express').Router()
const userGamesController = require('../controllers/userGamesController')
const restrict = require('../middleware/restrict');
const { UserGames } = require('../models')

router.get('/', (req, res) => {
    res.render('index')
})

router.get('/register', (req, res) => {
    res.render('register')
})

router.post('/register', userGamesController.createUser)

router.get('/login', (req, res) => {
    res.render('login')
})

router.post('/login', userGamesController.authUser)

router.get('/user', restrict, (req, res) => {
    res.render('biodata')
})

router.post('/user', )

router.get('/profile', restrict, (req, res) => {
    return res.json(req.UserGames.username)
})

module.exports = router