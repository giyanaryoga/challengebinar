const express = require('express');
const app = express()
const session = require('express-session');
const flash = require('express-flash');
const PORT = process.env.PORT || 8000;
const passport = require('./lib/passport')
const userGameRouter = require('./routers/userGamesRouter')

app.set('view engine', 'ejs')

// app.use(express.static(path.join(__dirname, 'public')))

app.use(express.urlencoded({extended: false}))

app.use(passport.initialize())

//routers
app.use(userGameRouter)

app.listen(PORT, () => {
    console.log("Server started on port", PORT)
})