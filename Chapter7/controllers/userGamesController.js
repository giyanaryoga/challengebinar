const { UserGames, UserBiodata, UserHistories } = require('../models')

jwtResponsesFormatter = (UserGames) => {
    let format = {
        id : UserGames.id,
        username : UserGames.username,
        accessToken : UserGames.generateToken()
    }
    return format
}

module.exports = {
    list : async (req, res) => {
        try{
            UserGames.findAll({
                include: [{
                    model: UserBiodata,
                    key: 'user_id'
                },{
                    model: UserHistories,
                    key: 'user_id'
                }],
                order: [
                    ['id', 'DESC']
                ]
            })
            .then(() => {
                res.status(200).json("Berhasil")
                res.redirect('/dashboard')
            })
        }catch(err){
            res.status(500).json(err)
        }
    },

    listById : async (req, res) => {
        try{
            const id = req.params.id
            UserGames.findOne({
                where: {id: id},
                include: [{
                    model: UserBiodata,
                    key: 'user_id'
                },{
                    model: UserHistories,
                    key: 'user_id'
                }]
            }).then(usergames => {
                if(!usergames){
                    return res.status(400).json("User Not Found")
                }
                return res.status(200).json(usergames)
            })
        }catch(err){
            res.status(500).json(err)
        }
    },

    createUser : async (req, res) => {
        try {
            let userCreated = await UserGames.register(req.body.username, req.body.password)
            return res.redirect('/login')
        } catch(err) {
            res.status(500).json(err)
        }
    },

    authUser : async (req, res) => {
        try {
            let checkUser = await UserGames.authentication(req.body.username, req.body.password)
            const id = UserGames.id
            res.redirect('/user')
            return res.status(200).json(jwtResponsesFormatter(checkUser))
        } catch(err) {
            res.status(500).json(err)
        }
    },

    update: async (req, res) => {
        try{
            const id = req.params.id
            UserGames.findOne({
                where: {id: id},
                include: [{
                    model: UserBiodata,
                    key: 'user_id'
                },{
                    model: UserHistories,
                    key: 'user_id'
                }]
            })
            .then()
        }catch(err){
            res.status(500).json(err)
        }
    },

    delete: async (req, res) => {
        try{
            const id = req.params.id
            UserGames.findOne({
                where: {id: id},
                include: [{
                    model: UserBiodata,
                    key: 'user_id'
                },{
                    model: UserHistories,
                    key: 'user_id'
                }]
            }).then(usergames => {
                if (!usergames) {
                    return res.status(400).json('Data Not Found')
                }
                return usergames
                .destroy()
                .then(usergames => {
                    res.status(200).json("Berhasil", usergames)
                    // res.redirect('/dasboard')
                })
            })
        }catch(err){
            res.status(500).json(err)
        }
    }
}