const { userhistories } = require("../models")

module.exports = {
    add : async (req, res) => {
        try{
            userhistories.create({
                waktu: req.body.waktu,
                skor: req.body.skor
            }).then(userhistories => {
                res.status(200).json("Berhasil", userhistories)
                res.redirect('/login')
            })
        }catch(err) {
            res.json(500, err)
        }
    },

    update : async (req, res) => {
        try{
            const id = req.params.id
            userhistories.findOne({
                where: {user_id: id},
                include: [{
                    model: userhistories,
                    key: 'user_id'
                }]
            }).then(userhistories => {
                if (!userhistories) {
                    return res.status(400).json('Data Not Found')
                }
                return userhistories
                .update({
                    waktu: req.body.waktu,
                    skor: req.body.skor
                }).then(userhistories => {
                    res.status(200).json("Berhasil", userhistories)
                    // res.redirect('/dasboard')
                })
            })
        }catch(err){
            res.json(500, err)
        }
    },

    delete : async (req, res) => {
        try{
            const id = req.params.id
            userhistories.findOne({
                where: {user_id: id},
                include: [{
                    model: userhistories,
                    key: 'user_id'
                }]
            }).then(userhistories => {
                if (!userhistories) {
                    return res.status(400).json('Data Not Found')
                }
                return userhistories
                .destroy()
                .then(userhistories => {
                    res.status(200).json("Berhasil", userhistories)
                    // res.redirect('/dasboard')
                })
            })
        }catch(err){
            res.json(500, err)
        }
    }
}