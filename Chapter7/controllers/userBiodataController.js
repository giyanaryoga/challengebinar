const { usergames, userbiodata } = require("../models")

module.exports = {
    listById: async (req, res) => {
        try{
            const id = req.params.id
            userbiodata.findOne({
                where: {user_id: id}
            })
            .then(userbiodata => {
                res.status(200).json(userbiodata)
            })
        }catch(err){
            res.json(500, err)
        }
    },

    add : async (req, res) => {
        try{
            userbiodata.create({
                nama: req.body.nama,
                alamat: req.body.alamat
            }).then(userbiodata => {
                res.status(200).json("Berhasil", userbiodata)
                res.redirect('/login')
            })
        }catch(err) {
            res.json(500, err)
        }
    },

    update : async (req, res) => {
        try{
            const id = req.params.id
            userbiodata.findOne({
                where: {user_id: id},
                include: [{
                    model: userbiodata,
                    key: 'user_id'
                }]
            }).then(userbiodata => {
                if (!userbiodata) {
                    return res.status(400).json('Data Not Found')
                }
                return userbiodata
                .update({
                    nama: req.body.nama,
                    alamat: req.body.alamat
                }).then(userbiodata => {
                    res.status(200).json("Berhasil", userbiodata)
                    // res.redirect('/dasboard')
                })
            })
        }catch(err){
            res.json(500, err)
        }
    },

    delete : async (req, res) => {
        try{
            const id = req.params.id
            userbiodata.findOne({
                where: {user_id: id},
                include: [{
                    model: userbiodata,
                    key: 'user_id'
                }]
            }).then(userbiodata => {
                if (!userbiodata) {
                    return res.status(400).json('Data Not Found')
                }
                return userbiodata
                .destroy()
                .then(userbiodata => {
                    res.status(200).json("Berhasil", userbiodata)
                    // res.redirect('/dasboard')
                })
            })
        }catch(err){
            res.json(500, err)
        }
    }
}