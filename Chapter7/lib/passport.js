const passport = require('passport')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const { usergames } = require('../models')

//Passport JWT option
const option = {
    //ngasih tau get jwt darimana
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    //secret yg kita punya
    secretOrKey: "secret",
}

passport.use(new JwtStrategy(option, async(playload, done) => {
    //tindakan selanjutnya
    usergames.findByPK(playload.id)
        .then(usergames => done(null, usergames))
        .catch(err => done(err, false))
}))

module.exports = passport


// Request => (Middleware) => Responses


// 1. Format jwt => Handnle Passport JWT
// 2. Key udah bener apa belum  => Handle passport JWT

// 3. Check username / id dari payload