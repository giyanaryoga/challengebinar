const { render } = require('ejs')
const express = require('express')
const appUser = express.Router()
const { UserGames, UserBiodata, UserHistories } = require('../models')

//form login
appUser.get('/', (req, res) => {
    res.render('./login')
})

//static auth login
appUser.post('/', (req, res) => {
    const {username, password} = req.body

    //validasi super user authentication
    if(username === "admin" && password === "admin"){
        return res.redirect('/user')
    } else{
        res.redirect('/')
        return res.status(400).json("Username atau password yang dimasukkan salah!")
    }
})

//menampilkan table semua user
appUser.get('/user', (req, res) => {
    UserGames.findAll({
        include: [{
            model: UserBiodata,
            as: 'UserBiodata'
        }, {
            model: UserHistories,
            as: 'UserHistories'
        }]
    })
    .then((usergames) => {
        res.render('./index', {usergames})

        // res.status(200).json(usergames)
    })
    .catch(err => {
        res.status(500).json(err)
    })
})

//form create user
appUser.get('/api/user', (req, res) => {
    res.render('./create')
})

//create user
appUser.post('/api/user', (req, res, next) => {
    const {username, password, nama, alamat, waktu, skor} = req.body

    if (username === undefined || password === undefined || nama === undefined || alamat === undefined || waktu === undefined || skor === undefined)
        return res.status(400).json("Mohon lengkapi dan isi semua form!")
    
    UserGames.create({
        username: username,
        password: password,
        UserBiodata: {
            nama: nama,
            alamat: alamat
        },
        UserHistories: {
            waktu: waktu,
            skor: skor
        }
    }, {
        include: [{
            model: UserBiodata,
            as: 'UserBiodata'
        }, {
            model: UserHistories,
            as: 'UserHistories'
        }]
    })
    .then(usergames => {
        res.status(200).json(usergames)
    }).catch(err => {
        res.status(500).json(err)
    })
})

//form update user
appUser.get('/api/user/:id', (req, res) => {
    UserGames.findOne({
        where: {
            id: req.params.id
        },
            include: [{
                model: UserBiodata,
                as: 'UserBiodata'
            }, {
                model: UserHistories,
                as: 'UserHistories'
            }]
    }).then(usergames => {
        res.render('./update', {usergames})
    }).catch(err => {
        res.status(500).json(err)
    })

    // res.render('./update')
})

//update user
appUser.put('/api/user/:id', (req, res) => {
    const {username, password, nama, alamat, waktu, skor} = req.body

    if (username === undefined || password === undefined || nama === undefined || alamat === undefined || waktu === undefined || skor === undefined){
        return res.status(400).json("Mohon lengkapi dan isi semua form!")
    }

    const whereClause = {
        where: {id : req.params.id}
    }

    UserGames.findOne(whereClause)
    .then(usergames => {
        return usergames
        .update({
            username: username,
            password: password,
            UserBiodata : {
                nama: nama,
                alamat: alamat
            },
            UserHistories : {
                waktu: waktu,
                skor: skor
            }
        }, {
            include: [{
                model: UserBiodata,
                as: 'UserBiodata'
            }, {
                model: UserHistories,
                as: 'UserHistories'
            }]
        })
        .then(() => {
            res.redirect('/user')
        })
    })
    .catch(err => {
        res.status(500).json(err)
    })
})

//delete user
appUser.delete('/api/clear/:id', (req, res) => {
    UserGames.destroy({
        where : {
            id : req.params.id,
            UserBiodataId: UserBiodata.id,
            UserHistoryId: UserHistories.id
        }
    }, {
        include: [{
            model: UserBiodata,
            key: 'UserBiodataId'
        }, {
            model: UserHistories,
            key: 'UserHistoryId'
        }]
    })
    .then(usergames => {
        res.status(200).json(usergames)
    }).catch(err => {
        res.status(500).json(err)
    })
})

module.exports = appUser