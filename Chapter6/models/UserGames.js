'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGames extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      UserGames.belongsTo(models.UserBiodata, {foreignKey: 'UserBiodataId', targetKey: 'id', as: 'UserBiodata'})
      UserGames.belongsTo(models.UserHistories, {foreignKey: 'UserHistoryId', targetKey: 'id', as: 'UserHistories'})
    }
  };
  UserGames.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserGames',
  });
  return UserGames;
};