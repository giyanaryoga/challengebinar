const express = require('express')
const method = require('method-override')
const bodyParser = require('body-parser')
const app = express()
const appUser = require('./routers/userRouter')

// app.use(
//     express.urlencoded({
//         extended: false
//     })
// )

app.use(bodyParser.urlencoded({
            extended: false
        }))

app.use(express.json())

app.use(method('_method'))

app.set('view engine', 'ejs')

app.use(appUser)

// router(app, db)

app.listen(3000, (req, res) => {
    console.log("Server is running!")
})