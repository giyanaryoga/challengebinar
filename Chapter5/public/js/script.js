/* DOM refresh */
const refresh = document.getElementById('restart')
  refresh.addEventListener('click', function (r) {
    location.reload()
})

class Suit{
  constructor(Batu, Gunting, Kertas, batu, gunting, kertas){
      this.cBatu = Batu
      this.cGunting = Gunting
      this.cKertas = Kertas
      this.pBatu = batu
      this.pGunting = gunting
      this.pKertas = kertas
  }

  pilihanComp() {
    const pilihComp = document.querySelectorAll('.pilihanComp')
    const Batu = document.querySelector('.Batu')
    const Gunting = document.querySelector('.Gunting')
    const Kertas = document.querySelector('.Kertas')
    const random = Math.random()
    pilihComp.forEach(function (p) {
      p.className = 'pilihanComp'
    })
      if (random < 0.28){
        Batu.parentElement.classList.add('active')
        return Batu.className
      }
      else if (random >= 0.28 && random < 0.58){
        Gunting.parentElement.classList.add('active')
        return Gunting.className
      }
      else {
        Kertas.parentElement.classList.add('active')
        return Kertas.className
      }
  }
}

//class void hasil untuk menampilkan tulisan draw, compWin, playerWin
class Hasil{
  Draw(){
    let draw = document.getElementById('result')
    draw.innerText = 'DRAW'
    draw.style.backgroundColor = '#035B0C'
    draw.style.textAlign = 'center'
    draw.style.color = '#fff'
    draw.style.fontWeight = '500'
    draw.style.fontSize = '36px'
    draw.style.fontFamily = 'arial'
    draw.style.paddingTop = '35px'
  }

  CompWin(){
    let cWin = document.getElementById('result')
    cWin.innerHTML = 'COM WIN'
    cWin.style.backgroundColor = '#4C9654'
    cWin.style.textAlign = 'center'
    cWin.style.color = '#fff'
    cWin.style.fontWeight = '500'
    cWin.style.fontSize = '36px'
    cWin.style.fontFamily = 'arial'
    cWin.style.paddingTop = '10px'
    cWin.style.paddingLeft = '20px'
    cWin.style.paddingRight = '20px'
    cWin.style.height = '130px'
  }

  PlayerWin(){
    let pWin = document.getElementById('result')
    pWin.innerText = 'PLAYER WIN'
    pWin.style.backgroundColor = '#4C9654'
    pWin.style.textAlign = 'center'
    pWin.style.color = '#fff'
    pWin.style.fontWeight = '500'
    pWin.style.fontSize = '36px'
    pWin.style.fontFamily = 'arial'
    pWin.style.paddingTop = '10px'
    pWin.style.height = '130px'
  }

  getHasil(player, comp) {
    //draw
    if (player === 'batu' && comp === 'Batu') { this.Draw() /*console.log('draw')*/}
    if (player === 'gunting' && comp === 'Gunting') {this.Draw()}
    if (player === 'kertas' && comp === 'Kertas') {this.Draw()}
    //lose
    if (player === 'gunting' && comp === 'Batu') {this.CompWin() /*console.log('lose')*/} 
    if (player === 'kertas' && comp === 'Gunting') {this.CompWin()}
    if (player === 'batu' && comp === 'Kertas') {this.CompWin()}
    //win
    if (player === 'kertas' && comp === 'Batu') {this.PlayerWin() /*console.log('win')*/}
    if (player === 'batu' && comp === 'Gunting') {this.PlayerWin()}
    if (player === 'gunting' && comp === 'Kertas') {this.PlayerWin()}
  }
}

//Objek class Suit
let suit = new Suit()
//Objek class Hasil
let hasil = new Hasil()

//Pilih Player dan memanggil objek suit dan objek hasil
const player = document.querySelector('#player')
const pilih = document.querySelectorAll('.pilihPlayer')

  player.addEventListener('click', function (e) {
    const pilihPlayer = e.target.className
    const pilihComp = suit.pilihanComp()
      pilih.forEach(function (p) {
        p.className = 'pilihPlayer'
      })
    e.target.parentElement.classList.add('active')
    if (e.target.parentElement.parentElement.classList.contains('active')){
      e.classList.remove('active')
    }
    console.log('Pilihan player : ', pilihPlayer)
    console.log('Pilihan computer : ', pilihComp)
    console.log('Hasil : ', hasil.getHasil(pilihPlayer, pilihComp))
  })
