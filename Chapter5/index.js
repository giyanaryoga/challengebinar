const express = require('express')
const app = express()

const port = 3000
const loginRouter = require('./routers/loginRouter')

app.use(express.json())

app.use(loginRouter)

// app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/js', express.static(__dirname + 'public/js'))
app.use('/img', express.static(__dirname + 'public/img'))

app.set('view engine', 'ejs')

// Menampilkan HTML login
app.get ('/api/backend/login', (req, res) => {
    app.render('pages/login')
})

app.get('/', (req, res) => {
    res.render('pages/index')
})

app.get('/games', (req, res) => {
    res.render('pages/games')
})

app.listen(port, () => {
    console.info(`Listening on port ${port}`)
})