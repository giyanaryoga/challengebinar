const express = require("express");
const router = express.Router()

let login = [
    {
        idUser : "A1",
        username : "admin",
        pass : "admin",
        nama : "Admin",
        level : "bos"
    },
    {
        idUser : "A2",
        username : "admin2",
        pass : "admin2",
        nama : "Admini saja",
        level : "karyawan"
    }
]

router.post('/api/backend/login', (req, res) => {
    const {username, pass, nama, level} = req.body

    if (username === undefined || pass === undefined || nama === undefined || level === undefined){
        res.status(400).json("Bad request!")
        return
    }

    const newId = login.length + 1
    const id = "A"+newId

    login.push({
        idUser : id,
        username : username,
        pass : pass,
        nama : nama,
        level : level
    })

    res.status(200).json("Username and Password success!")
})

router.get('/api/backend/login/', (req, res) => {
    res.status(200).json(login)
})

router.get('/api/backend/login/:idUser', (req, res) => {
    const id = req.params.idUser
    const filterAdmin = login.find(i => i.idUser == id)

    if (filterAdmin === undefined){
        res.status(400).json("Data User is not found!")
        return
    }
    
    res.status(200).json(filterAdmin)

})

router.put('/api/backend/login/:idUser', (req, res) => {
    const id = req.params.idUser
    const {username, pass, nama, level} = req.body

    if (username === undefined || pass === undefined || nama === undefined || level === undefined){
        res.status(400).json("Bad request")
    }

    let found = false
    for (let i = 0; i<login.length; i++){
        if (login[i].idUser == id){
            login[i].username = username,
            login[i].pass = pass,
            login[i].nama = nama,
            login[i].level = level
            found = true
            break
        }
    }

    if (found == true){
        res.status(200).json("Updated!")
        return
    } else {
        res.status(400).json("Bad request!")
        return
    }
})

router.delete('/api/backend/login/:idUser', (req, res) => {
    const id = req.params.idUser
    const searchIdUser = login.find(i => i.idUser == id)

    if (searchIdUser === undefined){
        res.status(400).json("Data User is not found!")
        return
    }

    const index = login.indexOf(searchIdUser)

    login.splice(index, 1)
    res.status(200).json("Deleted!")
})

module.exports = router